﻿using RSignal.Domain.Dtos;
using RSignal.Domain.Entities;

namespace RSignal.Application.UnitTests.Services.FakeData;

public static class BlogFakeDataHelper
{
    public static List<Blog> GetFakeBlogsList() 
    {
        return
        [
            new Blog { Id = 1, Name = "Test Name 1", Description = "Test Description 1" },
            new Blog { Id = 2, Name = "Test Name 2", Description = "Test Description 2" },
            new Blog { Id = 3, Name = "Test Name 3", Description = "Test Description 3" }
        ];
    }

    public static List<BlogDto> GetFakeBlogDtosList()
    {
        return GetFakeBlogsList().Select(b => new BlogDto { Id = b.Id, Name = b.Name, Description = b.Description }).ToList();
    }
}

