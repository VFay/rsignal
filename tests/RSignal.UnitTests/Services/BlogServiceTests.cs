﻿using FluentAssertions;
using MockQueryable.NSubstitute;
using NSubstitute;
using RSignal.Application.Blogs.Services;
using RSignal.Application.Common.Interfaces;
using RSignal.Application.UnitTests.Services.FakeData;

namespace RSignal.Application.UnitTests.Services;

public class BlogServiceTests
{
    private readonly IBlogService _blogService;

    public BlogServiceTests() 
    {
        var blogMock = Substitute.For<IApplicationDbContext>();
        var mockBlogDbSet = BlogFakeDataHelper.GetFakeBlogsList().BuildMock().BuildMockDbSet();

        blogMock.Blogs.Returns(mockBlogDbSet);

        _blogService = new BlogService(blogMock);
    }

    [Fact]
    public async Task ShouldReturnAllBlogs() 
    {
        var blogs = await _blogService.GetBlogs(CancellationToken.None);

        blogs.Should().HaveCount(BlogFakeDataHelper.GetFakeBlogDtosList().Count);
    }

    [Fact]
    public async Task ShouldReturnCorrectBlogId()
    {
        const int BlogId = 2;

        var expectedBlog = BlogFakeDataHelper.GetFakeBlogDtosList().SingleOrDefault(b => b.Id == BlogId);
        var actualBlog = await _blogService.GetBlogById(BlogId, CancellationToken.None);

        actualBlog.Should().Be(actualBlog);
    }
}