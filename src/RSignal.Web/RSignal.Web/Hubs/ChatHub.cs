﻿using Microsoft.AspNetCore.SignalR;
using RSignal.Web.Hubs.Interfaces;

namespace RSignal.Web.Hubs;

public class ChatHub : Hub<IChatClient>
{
    public async Task SendMessage(string user, string messsage) 
    {
        await Clients.All.ReceiveMessage(user, messsage);
    }
}