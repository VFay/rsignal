﻿namespace RSignal.Domain.Entities;

public class Blog
{
    public long Id { get; set; }

    public required string Name { get; set; }

    public required string Description { get; set; }
}
