﻿namespace RSignal.Domain.Dtos;

public class BlogDto
{
    public long Id { get; set; }

    public required string Name { get; set; }

    public required string Description { get; set; }
}