﻿using Microsoft.Extensions.DependencyInjection;
using RSignal.Application.Blogs.Services;

namespace RSignal.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services) 
    {
        services.AddScoped<IBlogService, BlogService>();

        return services;
    }
}