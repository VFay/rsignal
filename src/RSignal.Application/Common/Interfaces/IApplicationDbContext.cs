﻿using Microsoft.EntityFrameworkCore;
using RSignal.Domain.Entities;

namespace RSignal.Application.Common.Interfaces;

public interface IApplicationDbContext
{
    DbSet<Blog> Blogs { get; }
}