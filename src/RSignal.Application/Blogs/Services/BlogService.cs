﻿using Microsoft.EntityFrameworkCore;
using RSignal.Application.Common.Interfaces;
using RSignal.Domain.Dtos;

namespace RSignal.Application.Blogs.Services;

public class BlogService(IApplicationDbContext applciationDbContext) : IBlogService
{
    private readonly IApplicationDbContext _applciationDbContext = applciationDbContext;

    public async Task<IEnumerable<BlogDto>> GetBlogs(CancellationToken cancellationToken)
    {
        return await _applciationDbContext.Blogs.Select(x => new BlogDto
        {
            Id = x.Id,
            Name = x.Name,
            Description = x.Description,
        }).ToListAsync(cancellationToken);
    }

    public async Task<BlogDto?> GetBlogById(long id, CancellationToken cancellationToken)
    {
        return await _applciationDbContext.Blogs.Select(x => new BlogDto
        {
            Id = x.Id,
            Name = x.Name,
            Description = x.Description,
        }).SingleOrDefaultAsync(b => b.Id == id, cancellationToken);
    }
}