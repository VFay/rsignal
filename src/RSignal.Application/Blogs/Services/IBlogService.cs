﻿using RSignal.Domain.Dtos;

namespace RSignal.Application.Blogs.Services;

public interface IBlogService
{
    Task<IEnumerable<BlogDto>> GetBlogs(CancellationToken cancellationToken);

    Task<BlogDto?> GetBlogById(long id, CancellationToken cancellationToken);
}